import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendedTableExampleComponent } from './extended-table-example.component';

describe('ExtendedTableExampleComponent', () => {
  let component: ExtendedTableExampleComponent;
  let fixture: ComponentFixture<ExtendedTableExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendedTableExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendedTableExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
