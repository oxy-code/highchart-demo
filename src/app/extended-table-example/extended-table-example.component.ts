import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-extended-table-example',
  templateUrl: './extended-table-example.component.html',
  styleUrls: ['./extended-table-example.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ExtendedTableExampleComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
  expandedElement: any;
  errorMessage: string;

  constructor() { }

  ngOnInit() {
  }

  toggleExpandableRow(element: PeriodicElement): void {
    this.expandedElement = this.expandedElement === element ? {} : element;
    if (!('exclusions' in this.expandedElement) || this.expandedElement.exclusions.length === 0) {
      this.addEntryInExclusion(this.expandedElement);
    }
  }

  updateExclusion(element: PeriodicElement): void {
    const uniqueField = 'description';
    const uniqueValues = element.exclusions
      .map(item => item[uniqueField]).filter((v) => v.length) // removing empty values
      .filter((v, index, self) => self.indexOf(v) === index); // return only unique values
    if (element.exclusions.length != uniqueValues.length) {
      this.errorMessage = `There are duplicate/empty entries in '${uniqueField}' field`
    }
    else {
      this.errorMessage = '';
    }
    console.log(element)
  }

  addEntryInExclusion(element: any): void{
    const entry = {description: '', condition: ''};
    if ('exclusions' in element) {
      element.exclusions.push(entry);
    }
    else {
      element['exclusions'] = [entry];
    }
  }

  removeEntryFromExclusion(element: any, key?: number): void{
    if (key === undefined || (element['exclusions'] && element['exclusions'].length === 1)){
      window.alert("You shouldn't delete all the entries");
    }
    else if ('exclusions' in element){
      element.exclusions.splice(key, 1);
    }
  }

}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  exclusions?: any[];
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    position: 1,
    name: 'Hydrogen',
    weight: 1.0079,
    symbol: 'H',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test3', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test4', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test5', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test6', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test7', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test8', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 2,
    name: 'Helium',
    weight: 4.0026,
    symbol: 'He',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 3,
    name: 'Lithium',
    weight: 6.941,
    symbol: 'Li',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 4,
    name: 'Beryllium',
    weight: 9.0122,
    symbol: 'Be',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 5,
    name: 'Boron',
    weight: 10.811,
    symbol: 'B',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 6,
    name: 'Carbon',
    weight: 12.0107,
    symbol: 'C',
    exclusions: [{
      description: 'test1', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }, {
      description: 'test2', condition: '#root.fuelType(\'PHEV\').contains(\'PLUG_IN_HYBRID\')'
    }]
  }, {
    position: 7,
    name: 'Nitrogen',
    weight: 14.0067,
    symbol: 'N',
  }
];