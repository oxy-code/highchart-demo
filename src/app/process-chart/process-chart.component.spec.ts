import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessChartComponent } from './process-chart.component';

describe('ProcessChartComponent', () => {
  let component: ProcessChartComponent;
  let fixture: ComponentFixture<ProcessChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
