export const chartOptions = {
    chart: {
      type: 'column',
      height: 700,
      width: 700
    },
    title: {
      text: 'Process Owner Chart'
    },
    exporting: {
      buttons: {
        contextButton: {
          menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG'],
        },
      },
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: [] // Categorical names
    },
    yAxis: {
      min: 0,
      title: {
        text: 'R/1000',
        style: {
          fontWeight: 'bold'
        }
      },
      stackLabels: {
          enabled: true
      }
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true
        }
      },
      series: {
        dataLabels: {
          formatter: function(){
            if (this.y > 0) return this.y; // to eliminate the 0 values to be plotted in chart
          }
        }
      }
    },
    series: [] // points to plot
  };