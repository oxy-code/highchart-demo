import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';
HC_exporting(Highcharts);

import { chartOptions } from './process-chart.config';
import * as sampleJSON from '../../assets/json/sample.json';

@Component({
  selector: 'app-process-chart',
  templateUrl: './process-chart.component.html',
  styleUrls: ['./process-chart.component.css']
})
export class ProcessChartComponent implements OnInit {
  private jsonSrc = (sampleJSON as any).default;
  /**
   * Configure data labels & colors
   */
  private settings = {
    'paretoR1000StatusA': {name: 'Contained/No planned PCA', color: 'orange'},
    'paretoR1000StatusB': {name: 'No containment/Planned PCA', color: 'blue'},
    'paretoR1000StatusG': {name: 'PCA Implemented and Prevent Recurrence Complete', color: 'green'},
    'paretoR1000StatusP': {name: 'PCA Implemented', color: 'purple'},
    'paretoR1000StatusR': {name: 'No containment/No planned PCA', color: 'red'},
    'paretoR1000StatusY': {name: 'Contained/Planned PCA', color: 'yellow'}
  };
  private options: any = chartOptions;

  constructor() { }

  ngOnInit() {
    const categories = this.jsonSrc
      // sorting the object based on paretoR1000Order to show chart categories in order
      .sort((a, b) => a.columns.paretoR1000Order > b.columns.paretoR1000Order  ? 1 : -1)
      // pull out only the paretoGroupName and create as separate array
      .map(i => i.columns.paretoGroupName);
    
    const series = Object.values(this.jsonSrc.reduce((a, i) => {
      if (i.columns) {
        const { paretoR1000StatusA, paretoR1000StatusB, paretoR1000StatusG, paretoR1000StatusP, paretoR1000StatusR, paretoR1000StatusY } = i.columns;
        if (!a['paretoR1000StatusA']) {
          a['paretoR1000StatusG'] = this.getDataSeriesObject('paretoR1000StatusG', paretoR1000StatusG);
          a['paretoR1000StatusP'] = this.getDataSeriesObject('paretoR1000StatusP', paretoR1000StatusP);
          a['paretoR1000StatusY'] = this.getDataSeriesObject('paretoR1000StatusY', paretoR1000StatusY);
          a['paretoR1000StatusA'] = this.getDataSeriesObject('paretoR1000StatusA', paretoR1000StatusA);
          a['paretoR1000StatusB'] = this.getDataSeriesObject('paretoR1000StatusB', paretoR1000StatusB);
          a['paretoR1000StatusR'] = this.getDataSeriesObject('paretoR1000StatusR', paretoR1000StatusR);
        } else {
          a['paretoR1000StatusG'].data.push({y: +Number(paretoR1000StatusG).toFixed(2), color: this.settings['paretoR1000StatusG'].color});
          a['paretoR1000StatusP'].data.push({y: +Number(paretoR1000StatusP).toFixed(2), color: this.settings['paretoR1000StatusP'].color});
          a['paretoR1000StatusY'].data.push({y: +Number(paretoR1000StatusY).toFixed(2), color: this.settings['paretoR1000StatusY'].color});
          a['paretoR1000StatusA'].data.push({y: +Number(paretoR1000StatusA).toFixed(2), color: this.settings['paretoR1000StatusA'].color});
          a['paretoR1000StatusB'].data.push({y: +Number(paretoR1000StatusB).toFixed(2), color: this.settings['paretoR1000StatusB'].color});
          a['paretoR1000StatusR'].data.push({y: +Number(paretoR1000StatusR).toFixed(2), color: this.settings['paretoR1000StatusR'].color});
        }
      }
      return a;
    }, {}));
    this.options.xAxis.categories = categories;
    this.options.series = series;
    Highcharts.chart('container', this.options);
  }

  private getDataSeriesObject(key: string, value: string){
    return {
      name: this.settings[key].name,
      color: this.settings[key].color,
      data: [{
        y: +Number(value).toFixed(2),
        color: this.settings[key].color
      }]
    };
  }
}
