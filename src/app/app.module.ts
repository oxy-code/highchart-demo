import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatTableModule, MatFormFieldModule, MatInputModule, MatButtonModule} from '@angular/material';

import { AppComponent } from './app.component';
import { ProcessChartComponent } from './process-chart/process-chart.component';
import { ExtendedTableExampleComponent } from './extended-table-example/extended-table-example.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ProcessChartComponent,
    ExtendedTableExampleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
